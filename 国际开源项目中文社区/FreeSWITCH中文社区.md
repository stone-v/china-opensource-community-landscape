# FreeSWITCH中文社区

![](./logo/fscn.png)


FreeSWITCH是一个开源的电话软交换平台，支持实时音视频通信及会议，支持SIP及WebRTC等多种通信协议。其主要开发语言是C，某些模块中使用了C++，以MPL1.1发布。

[FreeSWITCH中文社区](http://freeswitch.org.cn) 由杜金房先生于 2009 年创办，社区运营着微信公众号“FreeSWITCH-CN”，QQ群和微信群有数千人。自成立以来，FreeSWITCH中文社区发表了大量的博客文章，部分文章由杜金房先生结集整理到了《FreeSWITCH权威指南》中，并于2014年出版。

FreeSWITCH 中文社区有每年一度的[FreeSWITCH开发者沙龙](http://rts.cn/events/)，自2020年起暂改为线上活动，更名为RTSCon。除每年一度的FreeSWITCH开发者沙龙外，FreeSWITCH早年也尝试过FreeSWITCH oTo（线上线下）活动，如[2015年阿里巴巴站](https://mp.weixin.qq.com/s/PJu8HUpvw9z-SWk_d1o0QQ)、[2014年FreeSWITCH深圳酒会](http://mp.weixin.qq.com/mp/appmsg/show?search_click_id=16058044187182318214-1664287751927-8971038904&__biz=MjM5MzIwMzExMg==&appmsgid=200054494&itemidx=1&sign=8d375c559689fab84656171ca1d19d3b#wechat_redirect&scene=7%23rd)等。

FreeSWITCH中文社区以及社区网站主要由杜金房老师运营和维护。2022年社区网站改版，但旧版的页面都仍然保留并保持原样。FreeSWITCH中文社区是一个非盈利的开源技术社区，商业化部分转到[RTS社区](http://rts.cn)，从某种意义上讲，FreeSWITCH社区也算是RTS社区的联盟社区之一。

目前，FreeSWITCH在国内应用非常迅速和广泛，很多Asterisk用户（另一款类似的开源软件，比FreeSWITCH出现要早）也转而使用FreeSWITCH。三大运营商、各大互联网公司也纷纷使用FreeSWITCH。FreeSWITCH中文文社区最初联合北京信悦通科技做 FreeSWITCH 的技术培训，每年有数个公开的班次、在北、上、深等城市举办，由杜金房老师主讲，为国内培养了大批FreeSWITCH技术人才。后来FreeSWITCH培训由杜金房老师创办的烟台小樱桃科技运营，仍然服务于广大FreeSWITCH中文开发者和用户。


