## 社区介绍
<img src="logo/shengjian-logo.png" width="200">  

盛见开源社区是由盛见团队的开源项目作者、贡献者、维护者共同组成的开源社区，提供开源的区块链框架、数字藏品、区块链监控等系列开源产品及解决方案，同时，还进行开源技术内容发布、开源赛事参与、开源项目共创等开源活动，现已服务用户20万+。

## 开源贡献
区块链和开源天然耦合,盛见会一直坚持开源,拥抱开源：

* 先后参加中国区块链开发大赛、华为鲲鹏开发大赛等开源活动，吸引更多贡献者参与社区、产品建设；
* 获得OpenAtom XuperChain 2021优秀案例、第五届中国区区块链开发大赛北部赛区优秀奖、中国信息科技前沿大会优秀解决方案、中国信息科技前沿大会优秀服务商 等多个奖项，获得市场口碑双重认可；
* 参加开放原子第三期开源工作坊，积极参与开源技术讲座，发掘更多优秀的软件开发者；
* 主办、承办、协办OpenAtom XuperChain 开源技术沙龙（郑州站）等开源、公益性质分享活动，发布技术软文，开展线上直播，促进开源知识分享；
* 盛见社区形成良好的开源工具生态，提供包括盛见本身在内的完全开源免费、不限商用的各种研发常见问题的解决方案。

盛见社区工具生态包括开源数字藏品平台OpenNFT、开源百度开放网络钱包OpenNFT-Client、开源区块链监控平台ChainEye,为用户提供数字藏品跨链开源协议，拥抱监管的跨链数字藏品铸造、发行、流转平台，提供开源的联盟链监控平台,监控区联盟链的健康运行状态。  

  
<img src="images/shengjian-1.png" width="800">  
<img src="images/shengjian-2.jpg" width="800">  


## 开源项目
### OpenNFT
![OpenNFT logo](logo/OpenNFT-logo.png)  
OpenNFT是全球首个数字藏品跨链开源协议，拥抱监管的跨链数字藏品铸造、发行、流转平台。  
OpenNFT解决数字藏品的潜在风险，设立冷却期，禁止恶意炒作，对用户进行实名认证，接入备案受监管的区块链网络，内容审核及司法保全等。  
OpenNFT跨链协议，源码开源，提供数字藏品资产跟主流虚拟空间（引擎）交互SDK的基础设施+工具。    
OpenNFT目前已支持Baidu超级链开放网络、bsn-ddc、ETH、polygon、bsc等。兼容主流区块链的多个主流数字藏品智能合约协议。    

### OpenNFT-Client
OpenNFT-Client是OpenNFT合约的钱包插件,已打包为Chrome浏览器插件,也支持html渲染展示。私钥离线实现了转移资产、查询资产余额、查询交易等功能,支持私钥和助记词登录,目前已对接完成百度开放网络和XuperChain底链。

### ChainEye
![chaineye logo](logo/chaineye-logo.png)  
ChainEye是一款开源的联盟链监控平台，目前已经支持百度XuperChain,开箱即用的产品体验。  
支持区块链浏览器功能,支持区块查询、交易查询、块高查询、显示区块高度、交易数等；  
支持国产化环境,包括Anolis、OpenEluer、kylin、UOS、鲲鹏芯片、达梦数据库等;  
支持 Docker、Helm Chart 等多种部署方式，内置多种监控大盘、快捷视图、告警规则模板，导入即可快速使用；    
支持 Categraf、Telegraf、Grafana-agent 等多种采集器，支持 Prometheus、VictoriaMetrics、M3DB 等各种时序数据库，支持对接 Grafana，与云原生生态无缝集成；  